# Multisensor

Multisensor based on a Wemos D1 Mini Pro

## Supplies

I buy everything from AliExpress. Prices are what I paid including shipping.

Total cost for one multisensor is about $9.

|        Part        | Purpose | Price | Notes |
|--------------------|---------|-------|-------|
| Wemos D1 Mini Pro  | Brain   | [$3.10](https://www.aliexpress.com/item/4001291931302.html) | The Pro was the cheapest I could find at the time.
| DHT22 Module       | Temperature/Humidity Sensor | [$3.28](https://www.aliexpress.com/item/32769460765.html)      | The DHT22 Module has built in resistors. |
| KY-013 3 pin       | Light Sensor | [$0.49](https://www.aliexpress.com/item/32820189174.html) | This also has a board with resistors. |
| AM312 PIR Sensor   | Motion Sensor | [$2.05](https://www.aliexpress.com/item/32921030810.html) | Cheap motion detector that works well.
| PLA Filament       | Enclosure material | $ | Parts designed by myself. Total weight of enclosure is ___g. |


## Wiring

Everything is powered by the 3.3v pin. Wires are soldered directly to the board and covered in hot glue.

![](images/wiring.png)

## ESPHome



## Enclosure

The enclosure is designed to fit all these parts inside.
![](images/packed.jpg)
![](images/birdseye.jpg)


### Mounting Options

Command strip, screw hole, etc.
